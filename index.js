const express = require('express')
const app = express()
const port = 3000
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.listen(port, () => console.log(`Server is running at localhost:${port}`))


// Create a GET route that will access the /home route that will print out a simple message.
// Process a GET request at the /home route using postman

app.get("/home", (request, response) => {
	response.send('Welcome to the home page!')
})

// Create a GET route that will access the /users route that will retrieve all the users in the mock database
// Process a GET request at the /users route using postman.

let users = []

app.get("/users", (request, response) => {
	{
		users.push(request.body)
		response.send(users)
	}
})

// Create a DELETE route that will access the /delete-user route to remove a user from the mock database.

let users2 = [
  	{
  		"username": "Alibaba",
  		"password": "dunba123"

  	},
  	{
  		"username": "Jasmine",
  		"password": "tiger543"
  	}

  ]

app.delete('/delete-user', (request, response) => {
  
  for(let i = 0; i < users2.length; i++){

		if(request.body.username == users2[i].username){
			users2.splice(i, 1)

			message = `User ${request.body.username} has been deleted!`

			break
		} else {
			message = "User does not exist!"
		}
	}

	response.send(message)
})


